#include <iostream>
using namespace std;

#include "Lista.h"

void Clear()
{
    cout << "\x1B[2J\x1B[H";
}

void mezclar(Lista *lista, Lista *final){
    Nodo *q;
    q = lista->p;
    while (q != NULL){
        final->agregar(q->numero);
        q = q->sig;
    }
}

void crear_lista(Lista *lista, int numero){
    Clear();
    int opc = 0;
    string line;
    
    do{ cout << "Lista" << endl;
        cout << "--------------------------" << endl;
        cout << "[1] Agregar" << endl;
        cout << "[2] Imprimir" << endl;
        cout << "[3] Terminar" << endl;
        cout << "--------------------------" << endl;
        cout << "Opción: ";
        cin >> opc;

        switch (opc)
        {
        case 1:
            Clear();
            cout << "Número: ";
            cin >> line;
            lista->agregar(stoi(line));
            break;
        case 2:
            Clear();
            lista->imprimir();
            break;
        case 3:
            Clear();
            break;
        default:
            break;
        }
    }
    while(opc != 3);
}

int main(){
    Clear();
    Lista *lista1 = new Lista();
    crear_lista(lista1, 1);


    Nodo *q = lista1->p;
    int fin;
    while (q != NULL){
        q = q->sig;
        if (q != NULL){
            fin = q->numero + 1;            
        }
    }
    int inicio = lista1->p->numero;
    q = lista1->p;
    bool esta;
    for (int i = inicio; i < fin; i++){
        esta = false;
        while (q != NULL){
            if (i == q->numero){
                esta = true;
            }
            q = q->sig;
        }
        if (esta != true){
            lista1->agregar(i);
        }
    }
    lista1->imprimir();
}