#ifndef LISTA_H
#define LISTA_H

#include <iostream>
using namespace std;

typedef struct _Nodo {
    int numero;
    struct _Nodo *sig;
} Nodo;

class Lista{
    public:
        Nodo *p = NULL;
        Lista();
        void agregar(int numero);
        void imprimir();
};
#endif