#include <iostream>
using namespace std;

#include "Lista.h"

void Clear()
{
    cout << "\x1B[2J\x1B[H";
}

void mezclar(Lista *lista, Lista *final){
    Nodo *q;
    q = lista->p;
    while (q != NULL){
        final->agregar(q->numero);
        q = q->sig;
    }
}

void crear_lista(Lista *lista, int numero){
    Clear();
    int opc = 0;
    string line;
    
    do{ cout << "Lista " << numero << endl;
        cout << "--------------------------" << endl;
        cout << "[1] Agregar" << endl;
        cout << "[2] Imprimir" << endl;
        cout << "[3] Terminar" << endl;
        cout << "--------------------------" << endl;
        cout << "Opción: ";
        cin >> opc;

        switch (opc)
        {
        case 1:
            Clear();
            cout << "Número: ";
            cin >> line;
            lista->agregar(stoi(line));
            break;
        case 2:
            Clear();
            lista->imprimir();
            break;
        case 3:
            Clear();
            break;
        default:
            break;
        }
    }
    while(opc != 3);
}

int main(){
    Clear();
    Lista *lista1 = new Lista();
    Lista *lista2 = new Lista();

    crear_lista(lista1, 1);
    crear_lista(lista2, 2);

    Lista *lista3 = new Lista();
    mezclar(lista1, lista3);
    mezclar(lista2, lista3);

    lista3->imprimir();

    delete lista1;
    delete lista2;
    delete lista3;
    return 0;
}