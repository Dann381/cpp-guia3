#include <iostream>
using namespace std;

#include "Lista.h"

void Clear()
{
    cout << "\x1B[2J\x1B[H";
}

int main(){
    Clear();
    int opc = 0;
    string line;
    Lista *lista = new Lista();
    
    do{ 
        cout << "--------------------------" << endl;
        cout << "[1] Agregar" << endl;
        cout << "[2] Imprimir" << endl;
        cout << "[3] Salir" << endl;
        cout << "--------------------------" << endl;
        cout << "Opción: ";
        cin >> opc;

        switch (opc)
        {
        case 1:
            Clear();
            cout << "Número: ";
            cin >> line;
            lista->agregar(stoi(line));
            break;
        case 2:
            Clear();
            lista->imprimir();
            break;
        case 3:
            Clear();
            cout << "Saliendo..." << endl;
            break;
        default:
            break;
        }
    }
    while(opc != 3);
    
    return 0;
}