# Guía 3

Los archivos contenidos en este repositorio contienen 3 ejercicios de c++ que están descritos en el pdf adjunto en el mismo repositorio. A continuación se dará una breve explicación de cada ejercicio.

### Ejercicio 1
Este ejercicio consiste en la creación de una lista enlazada simple ordenada, la cual es llenada con números que ingresará el usuario, para luego mostrarse los números ingresados de forma ordenada de menor a mayor. Este ejercicio contiene un pequeño menú con opciones para agregar números, ver la lista, y salir del programa.

### Ejercicio 2
Este ejercicio consiste en la creación de dos listas enlazadas ordenadas, para lo cual el usuario debe ingresar los números de ambas listas. Al terminar de ingresar los números, las listas se mezclarán en una sola lista que contendrá todos los números ingresados por el usuario ordenados de menor a mayor.

### Ejercicio 3
Este último ejercicio consiste en la creación de una lista enlazada simple ordenada, la cual será llenada por el usuario con números enteros. Al terminar de ingresar los números, el programa completará la lista con los números que faltan. Ejemplo: Números ingresados por el usuario -> 1, 3 y 6. Números que devuelve el programa -> 1, 2, 3, 4, 5 y 6.

### Ejecución
Cada ejercicio contiene su propio makefile. Para compilar cada ejercicio, es necesario entrar en su respectiva carpeta, y escribir en la terminal el comando "make". Esto dejará un archivo con el nombre del ejercicio, el cuál se puede ejecutar escribiendo el comando "./". Ejemplo: Si usted desea ejecutar el ejercicio 1, entre en la carpeta del ejercicio 1 mediante la terminal (use el comando "cd" para desplazarse por las carpetas), luego escriba el comando "make", y a continuación escriba el comando "./ejercicio1".

### Construido con
El presente proyecto de programación, se construyó en base al lenguaje de programación C++, en conjunto con el IDE "Visual Studio Code".

* [C++] - Lenguaje de programación utilizado.
* [Visual Studio Code](https://code.visualstudio.com/) - IDE utilizado.

### Autores
* **Daniel Tobar** - Estudiante de Ingeniería Civil en Bioinformática.
